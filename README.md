# Channeler

![logo_transparent](./phone/channeler/app/src/main/res/drawable/logo_transparent.png)

This application is a set of an android application [(Channeler)](https://gitlab.com/NithinSS/mediashare/-/releases/alpha-0.1) and a shell script(run.sh) for Linux that gives the following features:
1. Use android as a Speaker, Mic and Webcam for a Linux PC.
2. Use android biometrics to implement a two-step authentication for Linux



**STORY:**

My laptop became faulty and I was left with only a Desktop PC which had no Speaker, Mic  or Webcam and obviously I didn't want to buy them, that was the motivation to build this. This application was structured to be just presented as a final year project under KTU. After the successful submission and evaluation of this project now I intend to continue this application in a different direction.

## Steps to use the speaker:

**Status** :  OK  :)

1. Install the Channeler android application.
2. Download the 'run.sh' script from the pc/ folder .
3. On the PC in which you want to use the phone as speaker run the script like this:<br/> 
    `./run.sh -f speaker -a phone_address -p port_number` <br/>
   Note: You can do `./run.sh -h` for getting help.
4. In the app click the speaker button, the button will be yellow while connecting and will become green while a connection is established succesfully.
    If any error occurs then the button turns red. Then try again.
5. You can also connect multiple phones to a single PC and can have an effect of sorround speakers. 

This functionality uses GStreamer library end-to-end.



## Steps to use the Mic:

**Status** : PC side script is not complete, but  Channeler app can stream Mic input to PC. :(

1. Install the Channeler android application.

2. Download the 'run.sh' script from the pc/ folder.

3. On the PC in which you wan to receive phone's mic input, run the script like this :

   ` ./run.sh -f mic -p port_number`

   Note : You can do `./run.sh -h` for getting help.

4. In the app Go to the settings via the button on the top bar and Set the PC's IP and Port Number to use for Mic as given in the previous command on the PC.

5. Now go back to the main App window and Press the Mic button, It will immediately turn Green if no errors occur otherwise will become red. If error occurs then try again.

**NOTE** : Currently (v0.1-alpha) the PC side script will play the received audio via your PC's Speaker. This will be written to a Virtual Mic device via PulseAudio in the upcoming version. If you know how its done please create a new Issue and contribute it.

This functionality uses GStreamer library end-to-end.

## Steps to use Camera as Webcam :

**Status** : OK :)

1. Install the Channeler android application.

2. Download the 'run.sh' script from the pc/ folder.

3. **Setup V4L2Loopback Driver on PC**

   These Instructions might work for Debian and its derivatives, similar instructions are available for other distros easily.

   1. Do , `sudo apt install v4l2loopback-dkms` to install the V4L2 module.

   2. Insert the module as follows <br/> `sudo modprobe v4l2loopback devices=1 max_buffers=8 exclusive_caps=1 card_label="VirtualCam"`<br/> This will create a new device under /dev/videoX, you can explicitly specify X using option `video_nr=X`.

      Create multiple devices by giving the number of devices via option `devices=n`. This might be useful if you are going to use OBS-Studio. You can also remove the current configuration with `sudo modprobe -r v4l2loopback`.  I don't know a more graceful way to shut it down :/

4. On the PC run the script as follows : <br/> `./run.sh -f cam -p port_number`<br/> Other options like rotation are possible for Camera option, see `./run.sh -h` for all options.<br/> **NOTE**: The script currently (v0.1-alpha) writes to /dev/video0 if that's not the virtual device you created then change that part of the script to your device, in the pipeline in the cam() function. You can also test whether the camera feed is received by commenting the current pipeline in the cam() function and enabling the one below it.

5.  On the Channeler application, go to the Settings and give the PC's IP address and specify the port number for Webcam as same as you mentioned in Step 4. 

6. Now go back to the main page and click on the Camera button. Give permission if prompted. Then try again.

7. In the Camera Activity you can change the IP if required and Swap the Camera if required. Then press Start. If you see any glitch then try just switching back and forth between front and back cams. [This is a BUG :( ]

8. You can test this Camera feed via some online web cam test sites like [this](https://www.onlinemictest.com/webcam-test/)  or on OBS-Studio, or ffmpeg or any other application that can receive V4l2 sources.

**NOTE :** Online Conference sites like Google Meet, Jitsi Meet, Zoom etc. appears to be not supporting Virtual Cameras. Its probably because they query the Camera Device Abilities and manufacturer etc which a Virtual Camera does not provide. A work around is possible using how IP cams are setup probably using Newtek's NDI protocol. This ability is not yet added in v0.1-alpha. If you know how its done start a new issue and please contribute.



***NOTE : ** IPCam application on play store gives the above abilities with far more controls and maybe more reliability in some aspects. Channeler is a simpler application to achieve the same output, the amount of controls are kept low with respect to the KISS principle. Only simple things can grow infinitely :)

This functionality uses libstream on phone and GStreamer on PC.



## Steps to use Fingerprint for two step PAM Authentication in Linux :

**Status**: Partially working, not reliable or secure authentication :(

**WILL UPDATE THIS SOON AFTER ADDING THE PAM MODULE :)**





**WARNING: THIS PROJECT IS A WORK IN PROGRESS IF YOU WOULD LIKE TO CONTRIBUTE, THEN START AN ISSUE**<br/>
**THIS SOFTWARE IS PROVIDED AS IS WITHOUT ANY GUARANTEES**


