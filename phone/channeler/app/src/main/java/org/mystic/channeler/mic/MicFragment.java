package org.mystic.channeler.mic;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import org.mystic.channeler.R;
import org.mystic.channeler.StatusViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MicFragment extends Fragment {

    private final String TAG = "IOShareMic";
    private StatusViewModel mStatusModel;
    private BroadcastReceiver listener;
    private Observer<MicService.MicStatus> statusObserver;
    private final String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private final int mRequestCode = 200;


    public MicFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MicFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MicFragment newInstance(String param1, String param2) {
        MicFragment fragment = new MicFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusObserver = new Observer<MicService.MicStatus>() {
            @Override
            public void onChanged(MicService.MicStatus s) {
                // TODO: We need to update the ViewModel here
                //Log.d("Mic","The message received is "+s);
                // There should be a guarantee that the following will be called only after onViewCreated()
                // So we register observer only onViewCreated()
                ImageButton micBtn = (ImageButton) getView().findViewById(R.id.button_mic);
                switch(s){
                    case NONE: micBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null)); break;
                    case PAUSE:micBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_yellow,null));break;
                    case PLAYING: micBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_green,null)); break;
                    case ERROR: micBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_red,null)); break;
                    default: micBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null)); break;
                }
            }
        };
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStatusModel = new ViewModelProvider(requireActivity()).get(StatusViewModel.class);
        ImageButton micBtn = (ImageButton) getView().findViewById(R.id.button_mic);
        micBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(
                        getActivity(), Manifest.permission.RECORD_AUDIO) ==
                        PackageManager.PERMISSION_GRANTED)
                {
                    Intent startIntent = new Intent(getActivity(), MicService.class);
                    if(!MicService.micServiceRunning)
                        getActivity().startForegroundService(startIntent);
                    else getActivity().stopService(startIntent);
                } else {
                    requestPermissions(permissions,mRequestCode);
                }

            }
        });

        MicService.getCurrentStatus().observe(getActivity(), statusObserver);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case mRequestCode:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.d(TAG,"Granted permissions continue working...");
                    Toast.makeText(getActivity(), "Mic permission granted! You can start Mic now", Toast.LENGTH_LONG).show();
                } else {
                    Log.e(TAG,"Permission not given");
                    Toast.makeText(getActivity(), "Mic permissions should be given to continue", Toast.LENGTH_LONG).show();

                }
                return;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mic, container, false);

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}