package org.mystic.channeler.speaker;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.mystic.channeler.R;
import org.mystic.channeler.StatusViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpeakerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpeakerFragment extends Fragment {

    public StatusViewModel mStatusModel;
    private BroadcastReceiver listener;
    public Observer<SpeakerService.SpeakerStatus> statusObserver;

    public SpeakerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SpeakerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SpeakerFragment newInstance(String param1, String param2) {
        SpeakerFragment fragment = new SpeakerFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusObserver = new Observer<SpeakerService.SpeakerStatus>() {
            @Override
            public void onChanged(SpeakerService.SpeakerStatus s) {
                // TODO: We need to update the ViewModel here
                //Log.d("Speaker","The message received is "+s);
                // There should be a guarantee that the following will be called only after onViewCreated()
                // So we register observer only onViewCreated()
                ImageButton speakerBtn = (ImageButton) getView().findViewById(R.id.button_speaker);
                switch(s){
                    case NONE: speakerBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null)); break;
                    case PAUSE:speakerBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_yellow,null));break;
                    case PLAYING: speakerBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_green,null)); break;
                    case ERROR: speakerBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_red,null)); break;
                    default: speakerBtn.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null)); break;
                }
            }
        };
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStatusModel = new ViewModelProvider(requireActivity()).get(StatusViewModel.class);
        ImageButton speakerBtn = (ImageButton) getView().findViewById(R.id.button_speaker);
        speakerBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                Log.d("Speaker","ImageButton Clicked");
                Intent startIntent = new Intent(getActivity(), SpeakerService.class);
                if(!SpeakerService.speakerServiceRunning)
                    getActivity().startForegroundService(startIntent);
                else getActivity().stopService(startIntent);
            }
        });
        speakerBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // Give the dialog box to change settings
                DialogFragment newSpeakerDialog = new SpeakerDialogFragment();
                newSpeakerDialog.show(getActivity().getSupportFragmentManager(), null);
                return false;
            }
        });
        SpeakerService.getCurrentStatus().observe(getActivity(), statusObserver);
        //while(STATUS == null);
        Log.d("Speaker","Now status is "+SpeakerService.STATUS);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_speaker, container, false);

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}