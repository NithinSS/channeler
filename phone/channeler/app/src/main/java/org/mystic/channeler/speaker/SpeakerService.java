package org.mystic.channeler.speaker;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.freedesktop.gstreamer.GStreamer;
import org.mystic.channeler.ConnectionDetailsFragment;
import org.mystic.channeler.MainActivity;
import org.mystic.channeler.R;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

/*
This is the service that communicates with GStreamer using JNI to achieve Speaker
functionality. It sends status updates to the SpeakerFragment and status messages to
StatusMessageFragment.
 */


public class SpeakerService extends Service {
    // TODO: BUG <=> If a GStreamer error occurs then this service shouldn't start.
    // TODO: BUG <=> Starting and stopping service multiple times casues it to not work.

    // TODO: More than 99% of speaker and mic service are the same, so make them a single multi-threaded service
    private static final int NOTIFICATION_ID = 1;
    public static boolean speakerServiceRunning = false;

    // native methods
    protected native void nativeInit();
    protected static native boolean nativeClassInit();
    protected native void nativeFinalize();
    protected native void nativePlay();
    protected native void nativePause();

    protected String nativePipelineString;
    protected long native_custom_data;

    protected static boolean Initialized = false;
    protected boolean is_playing = false;
    public static MutableLiveData<SpeakerStatus> STATUS;
    public static enum SpeakerStatus {
        NONE, PAUSE, PLAYING, ERROR
    };
    public static MutableLiveData<SpeakerStatus> getCurrentStatus() {
        if (STATUS == null) {
            STATUS = new MutableLiveData<SpeakerStatus>();
        }
        return STATUS;
    }



    public SpeakerService() { }

    public int init(Context serviceContext){
        //Initialize GStreamer and warn if it fails
        try {
            GStreamer.init(serviceContext);
        } catch (Exception e) {
            return 1;
        }
        nativeClassInit();
        nativeInit();
        Initialized = true;
        return 0;
    }

    public boolean play(){
        nativePlay();
        is_playing = true;
        return true;
    }

    protected void onGStreamerInitialized(){

    }

    public boolean pause(){
        nativePause();
        is_playing = false;
        return true;
    }

    public void finalize(){
        Initialized = false;
        nativeFinalize();
    }

    @Override
    public void onCreate(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String port = prefs.getString("speaker_port","5000");
        Log.d("io-share","port is "+port);
        nativePipelineString = "udpsrc caps=\"application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00\" port="+port+
                " ! rtpjitterbuffer latency=200 ! rtpopusdepay ! opusdec plc=true ! openslessink";
        init(this);
        getCurrentStatus().setValue(SpeakerStatus.NONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);
        // TODO: Put a pause/stop button action on the notification
        Log.d("Speaker","Making the notification");
        Notification notification =
                new Notification.Builder(this, "channeler-notify")
                        .setContentTitle(getText(R.string.notification_title))
                        .setContentText(getText(R.string.notification_speaker_message))
                        .setSmallIcon(R.drawable.ic_speaker)
                        .setContentIntent(pendingIntent)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(NOTIFICATION_ID, notification);
        if(isPlaying()) pause();
        speakerServiceRunning = true;
        try {
            // wait till the native code initializes the pipeline
            Thread.sleep(10); 
            // TODO : Make pipeline variable thread-safe instead of hardcoding wait time

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        play();
        Toast.makeText(this, "Speaker service starting", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    public static boolean isInitialized(){
        return Initialized;
    }

    public boolean isPlaying(){
        return is_playing;
    }


    protected void setMessage(String Message){
        // Send status broadcast to the SpeakerFragment
        // Send status message broadcast to the MessageFragment
        // Everything this method does has to be done should be inside a try catch
        // otherwise the NativeMethod might throw an Error
        try {
            Log.d("Speaker", "The message we see here is " + Message);
            if (Message.contains("PAUSED")) getCurrentStatus().postValue(SpeakerStatus.PAUSE);
            else if (Message.contains("PLAYING")) getCurrentStatus().postValue(SpeakerStatus.PLAYING);
            if (Message.contains("ERROR") || (Message.contains("Error")) || (Message.contains("Unable to build") || (Message.contains("Unable to change"))))
                getCurrentStatus().postValue(SpeakerService.SpeakerStatus.ERROR);
        } catch (Exception e) {
            Log.e("Speaker","Error caught while setMessage()"+e.toString());
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy(){
        finalize();
        speakerServiceRunning = false;
        Toast.makeText(this, "Speaker service stopping", Toast.LENGTH_SHORT).show();
        getCurrentStatus().setValue(SpeakerStatus.NONE);
    }

    static {
        System.loadLibrary("speaker");
        System.loadLibrary("gstreamer_android");
    }

}
