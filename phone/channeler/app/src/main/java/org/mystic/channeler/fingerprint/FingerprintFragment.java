package org.mystic.channeler.fingerprint;

import androidx.biometric.BiometricPrompt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.mystic.channeler.R;
import com.jcraft.jsch.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executor;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FingerprintFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FingerprintFragment extends Fragment {


    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private String IP;

    public FingerprintFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment FingerprintFragment.
     */
    public static FingerprintFragment newInstance(String param1, String param2) {
        FingerprintFragment fragment = new FingerprintFragment();
        return fragment;
    }

    private class UserAuth extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Log.d("Authentication", "Trying to make connection");
                JSch jsch = new JSch();
                JSch.setConfig("StrictHostKeyChecking", "no");
                Session session = jsch.getSession("robey", IP, 2200);
                UserInfo ui = new UserInfo() {
                    String passwd;
                    @Override
                    public String getPassphrase() {
                        return null;
                    }

                    @Override
                    public String getPassword() {
                        return passwd;
                    }

                    //TODO: Authentication to be done using key
                    @Override
                    public boolean promptPassword(String message) {
                        passwd="foo";
                        return true;
                    }

                    @Override
                    public boolean promptPassphrase(String message) {
                        return false;
                    }

                    @Override
                    public boolean promptYesNo(String message) {
                        return false;
                    }

                    @Override
                    public void showMessage(String message) {
                        return;
                    }
                };
                session.setUserInfo(ui);
                session.connect();
                try {
                    Channel channel = session.openChannel("shell");
                    //InputStream iStream = new ByteArrayInputStream(ui.getPassword().getBytes(StandardCharsets.UTF_8));
                    //channel.setInputStream(iStream);
                    //OutputStream oStream = new OutputStream() {
                    //    @Override
                    //    public void write(int i) throws IOException {
                    //        Log.d("InOutputstream", "" + i);
                    //    }
                    //};
                    //channel.setOutputStream(oStream);
                    channel.connect(); // Just a connection is made and not the authentication
                }catch (JSchException J) {
                    Log.e("Authentication", "Some stupid error in channel : "+J);
                }
            } catch (Exception e ){Log.e("Authentication", "Some stupid error in tryAuth : "+e);}

            return null;
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fingerprint, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ImageButton fpButton = (ImageButton) getView().findViewById(R.id.button_fingerprint);
        executor = ContextCompat.getMainExecutor(getActivity());
        biometricPrompt = new BiometricPrompt(getActivity(), executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
                fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_red,null));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Change back to normal after 2s = 2000ms
                        fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null));
                    }
                }, 2000);
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                new UserAuth().execute();
                Toast.makeText(getActivity().getApplicationContext(),
                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_green,null));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Change back to normal after 2s = 1000ms
                        fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null));
                    }
                }, 1000);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getActivity().getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
                fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_red,null));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Change back to normal after 2s = 2000ms
                        fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle,null));
                    }
                }, 2000);
            }
        });




        // Prompt appears when user clicks "Fingerprint button"
        // Consider integrating with the keystore to unlock cryptographic operations,
        // if needed by your app.
        fpButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Function not stable!", Toast.LENGTH_SHORT).show();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                IP = prefs.getString("pc_ip"," ");
                fpButton.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.circle_yellow,null));
                if(IP.equals(" ")) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("PC not configured")
                            .setMessage("Please configure the PC in the settings")
                            .setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .show();
                } else {
                    promptInfo = new BiometricPrompt.PromptInfo.Builder()
                            .setTitle("Biometric login to "+IP)
                            .setSubtitle("Log in using your biometric credential")
                            .setNegativeButtonText("Cancel")
                            .build();
                    biometricPrompt.authenticate(promptInfo);
                }
            }
        });
    }
}