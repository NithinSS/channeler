package org.mystic.channeler.cam;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import org.mystic.channeler.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CamFragment extends Fragment {

    private final String TAG = "IOShareCam";
    private final String[] permissions = {Manifest.permission.CAMERA};
    private final int cRequestCode = 300;


    public CamFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CamFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CamFragment newInstance(String param1, String param2) {
        CamFragment fragment = new CamFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageButton camBtn = (ImageButton) getView().findViewById(R.id.button_cam);
        camBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(
                            getActivity(), Manifest.permission.CAMERA) ==
                            PackageManager.PERMISSION_GRANTED)
                    {
                        Intent startIntent = new Intent(getActivity(), CameraActivity.class);
                        startActivity(startIntent);
                    } else {
                        requestPermissions(permissions, cRequestCode);
                    }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case cRequestCode:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.d(TAG,"Granted permissions continue working...");
                    Toast.makeText(getActivity(), "Camera permission granted! You can start WebCam now", Toast.LENGTH_LONG).show();
                } else {
                    Log.e(TAG,"Permission not given");
                    Toast.makeText(getActivity(), "Camera permissions should be given to continue", Toast.LENGTH_LONG).show();

                }
                return;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cam, container, false);
    }
}