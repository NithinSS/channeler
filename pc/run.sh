#!/usr/bin/env bash

# This script might work only on debian and its derivatives
# If you just want the core functionality then use the pipelines themseleves (the commands that start with gst-launch)

VERSION_CODE="2"
VERSION_NAME="0.1.0"

run_install()
{
	## Prompt the user
	read -p "Do you want to install missing packages? [Y/n]: " answer
	## Set the default value if no answer was given
	answer=${answer:Y}
	## If the answer matches y or Y, install
	[[ $answer =~ [Yy] ]] && sudo apt-get install ${gstPackages[@]}
}

cam()
{
	gst-launch-1.0 -vvv udpsrc port=$1 ! 'application/x-rtp,payload=96,encoding-name=H264' ! rtph264depay ! decodebin ! videoflip video-direction=$2 ! videoconvert ! identity drop-allocation=1 ! v4l2sink device=/dev/video0
	#gst-launch-1.0 -v udpsrc port=$1 ! 'application/x-rtp,payload=96,encoding-name=H264' ! rtph264depay ! decodebin ! videoflip video-direction=$2 ! autovideosink
}
speaker()
{
	# current=$(amixer get Master| tail -1 | cut -f2 -d[ | cut -f1 -d%)
	# pactl set-sink-volume 0 1%
	# speaker pipeline
	gst-launch-1.0 -v pulsesrc ! audioconvert ! opusenc ! rtpopuspay ! udpsink port=$2 host=$1
	# amixer set Master $current%
}

mic()
{
	# mic pipeline
	AUDIO_CAPS="application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00"
	gst-launch-1.0 -vv udpsrc caps=$AUDIO_CAPS port=$1 buffer-size=50 ! rtpopusdepay ! opusdec plc=true ! alsasink
}

print_usage()
{
	echo "Usage:";
	echo "  ./run.sh -f <function> [options] Use a target device for various I/O functions"
	echo "Options:";
	echo "-h : Print this usage information";
	echo "-v : verbose";
	echo "-f <function> : Function to perform.Possible values are \"speaker\", \"mic\", \"fingerprint\", \"cam\"";
	echo "  *Only one function can be performed per instance, you can run multiple instances";
	echo "-a <address> : Target device's IP address";
	echo "-p <port> : Port number to use for this function";
	echo "-r <rotation> : Angle to rotate video one of [identity, 180, 90l, 90r]";
	echo "		Use -r to correct video streaming orientation";
}


# -------------- Script starts here------------------------------q
gstPackages=("gstreamer1.0-tools" "gstreamer1.0-plugins-base" "gstreamer1.0-plugins-good"
                "gstreamer1.0-plugins-bad" "gstreamer1.0-plugins-ugly" "gstreamer1.0-pulseaudio"
                "gstreamer1.0-alsa" "gstreamer1.0-plugins-rtp" "gstreamer1.0-rtsp")


if [[ $# -lt 1 ]]; then print_usage; exit 1; fi
while getopts 'f:a:p:r:vh' flag; do
	case "${flag}" in
		f) action="${OPTARG}" ;;
		a) ip="${OPTARG}" ;;
		p) port="${OPTARG}" ;;
		r) rotation="${OPTARG}" ;;
		v) verbose="true" ;;
		h) print_usage ; exit 1 ;;
		*) print_usage ; exit 1 ;;
	esac
done

case "$action" in 
	'speaker')    
		## Run the run_install function if any of the required packages are missing
		dpkg -s "${gstPackages[@]}" >/dev/null 2>&1 || run_install
		if [ -z ${ip+x} ]; then echo "Should specify a target ip address"; exit 1; fi
		if [ -z ${port+x} ]; then echo "Should specify a port number"; exit 1; fi
		echo "Using '$ip' as speaker..."
		if [ -z ${verbose+x} ]; then speaker $ip $port > /dev/null; else speaker $ip $port; fi
		printf "\nExiting...\n"
		;;
	'mic')
		## Run the run_install function if any of the required packages are missing
                dpkg -s "${gstPackages[@]}" >/dev/null 2>&1 || run_install
                if [ -z ${port+x} ]; then echo "Should specify a port number"; exit 1; fi
		echo "Our ip is '$(hostname -I)'"
		echo "Receving audio on port '$port'..."
                if [ -z ${verbose+x} ]; then mic $port > /dev/null; else mic $port; fi
                printf "\nExiting...\n"
		;;
	'fingerprint')
		echo "Feature not yet implemented"
		;;
	'cam')
		 ## Run the run_install function if any of the required packages are missing
                dpkg -s "${gstPackages[@]}" >/dev/null 2>&1 || run_install
                echo "Our ip is '$(hostname -I)'"
		if [ -z ${port+x} ]; then port=5006 ; echo "Using default port for video $port"; fi
		if [ -z ${rotation+x} ]; then rotation=90l ; echo "Using default rotation for video $roation"; fi
		echo "Rotating video for $rotation"
		echo "Receving video on $port ..."
                if [ -z ${verbose+x} ]; then cam $port $rotation > /dev/null; else cam $port $rotation; fi
                printf "\nExiting...\n"
		;;
	 *) echo "Invalid action"
	        ;;
esac


