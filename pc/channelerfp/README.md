## Authentication from Channeler

Move the files under this directory to ```/lib/security/channelerfp``` and in ```/etc/pam.d/login  ``` add  :

```auth   requisite      pam_python.so /lib/security/channelerfp/pam.py```

to the end as a module to enable channelerfp authentication on login.

When this is enabled, when you try to login first you have to enter your basic credentials, if it correct a phone based authentication is started as a second step authentication. Now on the channeler app press the fingerprint button and do the authentication and now the system will be unlocked. 

### Disclaimer :

This is an experimental technique and far from completion don't rely on this as a primary authentication

Errors in the pam-config will have serious impacts on the system. So do this only if you know what you are doing.

