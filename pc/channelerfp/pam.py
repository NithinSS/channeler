# PAM interface in python, launches authserver.py


# Import required modules
import subprocess
import sys
import os
import glob


def doAuth(pamh):
    """Starts authentication in a seperate process"""

    pamh.conversation(pamh.Message(
        pamh.PAM_TEXT_INFO, "Waiting for verification from Phone"))

    # Run compare as python3 subprocess to circumvent python version and import issues
    status = subprocess.call(["/usr/bin/python3", os.path.dirname(
        os.path.abspath(__file__)) + "/authserver.py", pamh.get_user()])

    # Status 10 means we couldn't find any face models
    if status == 10:
        return pamh.PAM_USER_UNKNOWN
    # Status 11 means we exceded the maximum retry count
    elif status == 11:
        pamh.conversation(pamh.Message(pamh.PAM_ERROR_MSG,
                                       "Verification time out reached"))
        return pamh.PAM_AUTH_ERR
    # Status 12 means we aborted
    elif status == 12:
        return pamh.PAM_AUTH_ERR
    # Status 0 is a successful exit
    elif status == 0:
        # Show the success message if it isn't suppressed

        pamh.conversation(pamh.Message(pamh.PAM_TEXT_INFO,
                                       "Received verification from phone for " + pamh.get_user()))

        return pamh.PAM_SUCCESS

    # Otherwise, we can't discribe what happend but it wasn't successful
    pamh.conversation(pamh.Message(pamh.PAM_ERROR_MSG,
                                   "Unknown error: " + str(status)))
    return pamh.PAM_SYSTEM_ERR


def pam_sm_authenticate(pamh, flags, args):
    """Called by PAM when the user wants to authenticate, in sudo for example"""
    pamh.conversation(pamh.Message(pamh.PAM_TEXT_INFO,
                                   "Starting authserver..."))
    return doAuth(pamh)


def pam_sm_open_session(pamh, flags, args):
    """Called when starting a session, such as su"""
    return doAuth(pamh)


def pam_sm_close_session(pamh, flags, argv):
    """We don't need to clean anyting up at the end of a session, so returns true"""
    return pamh.PAM_SUCCESS


def pam_sm_setcred(pamh, flags, argv):
    """We don't need set any credentials, so returns true"""
    return pamh.PAM_SUCCESS
